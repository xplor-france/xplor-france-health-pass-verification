import got, { Got, GotOptions } from 'got';
import config from '../config/config';
import ErrorAgent from '../middlewares/error-agent';

export enum healthPassStatus {
    VALID = 'Valide',
    INVALID = 'Non valide'
}

enum QrType {
    DCC = 'DCC',
    _2D_DOC = '2D-DOC'
}

class TacGouvApiProvider {
    httpClient:Got;

    constructor () {
        try {
            this.httpClient = got.extend({
                headers: {
                    Authorization: 'Bearer ' + config.TacGouvApi.token
                },
                responseType: 'json'
            });
        } catch (e) {
            ErrorAgent.captureException(e);
        }
    }

    DCCCheck (dccMessage: string) {
        const qrType = dccMessage.startsWith('DC') ? QrType._2D_DOC : QrType.DCC;
        return this.httpClient.post(`${config.TacGouvApi.url}${qrType}`, {
            body: dccMessage
        }).json();
    }
}

const tacGouvApiProvider = new TacGouvApiProvider();
export default tacGouvApiProvider;
