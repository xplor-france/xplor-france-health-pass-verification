'use strict';

import { HealthPass } from '../types/healthPassCheck.interface';
import tacGouvApiProvider, { healthPassStatus } from '../providers/tacGouvApi.provider';
import { UserType } from '../lib/api';
const {
    BookingRepository,
    MemberBookingRepository,
    HealthPassCheckRepository
} = require('../repositories');
const CONST = require('../utils/CONSTANTS');
const { DeciService } = require('../lib/services');
const { Options } = require('../utils/options');

/**
 * HealthPassCheck
 *
 * @summary Service TEMPORAIRE gérant la validation de pass sanitaire
 *
 * @description
 *
 *
 * @memberof module:Services
 * @author TBO
 */

class HealthPassCheck extends DeciService {
    get services () {
        return ['config', 'member'];
    }

    init () {
        this.bookingRepository = new BookingRepository(this.db, this.logger);
        this.memberBookingRepository = new MemberBookingRepository(this.db, this.logger);
        this.healthPassCheckRepository = new HealthPassCheckRepository(this.db, this.logger);
    }

    /**
     * Route temporaire pour la validation des Pass Sanitaire
     * @param memberId
     * @param healthPassList
     * @param bookingId
     */
    async healthPassCheck (memberId: number, healthPassList: Array<HealthPass>, bookingId = null) {
        const checkResults = [];
        let healthPassValidIndex = 0;
        const maxDateHealthPassCheck = this.toolbox.datetime.add(this.toolbox.datetime.now(), CONST.HEALTH_PASS.VALIDITY_DURATION, 'hours');
        for (const healthPass of healthPassList) {
            const apiResult: any = await tacGouvApiProvider.DCCCheck(healthPass.value).catch(err => {
                return { error: err?.response?.statusCode || 500 };
            });
            checkResults.push({ id: healthPass.id, value: healthPass.value, checkResponse: apiResult });
        }

        healthPassValidIndex = checkResults.findIndex(checkResult => this._checkIfHealthPassIsValid(checkResult?.checkResponse));
        const healthPassToCheck = healthPassValidIndex > 0 ? checkResults[healthPassValidIndex] : checkResults[0];
        if (this._checkIfHealthPassIsValid(healthPassToCheck?.checkResponse)) {
            await this.memberService.activateHealthPass({ memberId, temporary: false, date: maxDateHealthPassCheck });
        } else {
            await this.memberService.disableHealthPass(memberId);
        }

        // call gouv api for each bookingId
        let memberBookingList = [];
        if (bookingId) {
            const booking = await this.bookingRepository.getById(bookingId, new Options().includes(['resource']));
            if (!this.isEnabledForZone(booking.resource.idz)) {
                throw this.error('health pass check disabled', 403, { skipSentry: true });
            }

            const bookingDate = this.toolbox.datetime.convertBDDWithDecitimeToDatetime(booking.date, booking.startTime, '');
            if (this.toolbox.datetime.isAfter(bookingDate, maxDateHealthPassCheck)) {
                throw this.error(`Booking date is after ${maxDateHealthPassCheck.toString()}`, 403);
            }
            memberBookingList.push(await this.memberBookingRepository.findByMemberBookingOnSchedule(bookingId, memberId));
        } else {
            memberBookingList = await this.memberBookingRepository.findUpcomingByParamsWithIncludes({ memberId, to: maxDateHealthPassCheck }).then((memberBookings) => {
                return memberBookings.filter(memberBooking => this.isEnabledForZone(memberBooking.booking.resource.idz));
            });
        }

        memberBookingList = memberBookingList.filter(Boolean);

        for (const memberBooking of memberBookingList) {
            const metadata = healthPassToCheck?.checkResponse;
            if (UserType.Staff.equals(this.userDatas.api)) {
                metadata.userId = this.userDatas.id;
            }

            this.healthPassCheckRepository.upsert({
                id: memberBooking.id,
                isValid: this._checkIfHealthPassIsValid(healthPassToCheck?.checkResponse),
                metadata: JSON.stringify(metadata)
            });
        }
        return checkResults;
    }

    async manualHealthPassCheck (memberId: number, bookingId: number) {
        const booking = await this.bookingRepository.getById(bookingId, new Options().includes(['resource']));
        const isHealthPassEnabled = this.isEnabledForZone(booking.resource.idz);
        const maxDateHealthPassCheck = this.toolbox.datetime.add(this.toolbox.datetime.now(), CONST.HEALTH_PASS.VALIDITY_DURATION, 'hours');

        if (!isHealthPassEnabled) {
            throw this.error('healthPass-disabled', 403, { skipSentry: true });
        }

        return this.memberBookingRepository.findByMemberBookingOnSchedule(bookingId, memberId).then(memberBooking => {
            const metadata: any = { manualCheck: true };
            if (!memberBooking) {
                throw this.error('Booking member not found', 404);
            }
            if (UserType.Staff.equals(this.userDatas.api)) {
                this.memberService.activateHealthPass({ memberId, temporary: false, date: maxDateHealthPassCheck });
                metadata.userId = this.userDatas.id;
            } else {
                metadata.memberId = memberId;
            }

            this.healthPassCheckRepository.upsert({
                id: memberBooking.id,
                isValid: 1,
                metadata: JSON.stringify(metadata)
            });
            return { success: true };
        });
    }

    async getHealthPassValidityForMemberBooking (memberBooking) {
        let healthPassValid;
        const startBookingDate = this.toolbox.datetime.convertBDDWithDecitimeToDatetime(memberBooking.booking.date, memberBooking.booking.startTime);
        const maxDateHealthPassCheck = this.toolbox.datetime.add(this.toolbox.datetime.now(), CONST.HEALTH_PASS.VALIDITY_DURATION, 'hours');
        if (this.toolbox.datetime.isSameOrBefore(startBookingDate, maxDateHealthPassCheck)) {
            healthPassValid = await this.healthPassCheckRepository.findOne({ where: { id: memberBooking.id } }).then(result => {
                return result?.isValid || 0;
            });
        }
        return healthPassValid;
    }

    isEnabledForZone (zoneId: number) {
        let isEnabled = false;
        if (this.conf.health_pass_v2) {
            const healthPassConfig = JSON.parse(this.conf.health_pass_v2);
            if (healthPassConfig) {
                isEnabled = healthPassConfig[zoneId];
            }
        }

        return isEnabled;
    }

    private _checkIfHealthPassIsValid (apiResult) {
        return apiResult?.data?.dynamic[0]?.liteValidityStatus === healthPassStatus.VALID && !apiResult?.data?.static?.isBlacklisted;
    }
}

export = HealthPassCheck;
