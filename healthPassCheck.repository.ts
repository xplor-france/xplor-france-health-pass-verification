'use strict';

const Repository = require('../lib/repositories/DeciRepo.abstract');
const DeciServiceToolbox = require('../lib/toolbox/DeciService.toolbox.class');
const Sequelize = require('sequelize');

/**
 * Repository du modèle HealthPassCheck
 */
class HealthPassCheckRepository extends Repository {
    constructor (db, logger = undefined) {
        super(db, logger);
        this.toolbox = new DeciServiceToolbox(this.db);
    }

    get dbType () {
        return 'current';
    }

    get modelName () {
        return 'HealthPassCheck';
    }

    update (healthPassCheck) {
        return super.update(healthPassCheck, {
            where: {
                id: healthPassCheck.id
            }
        });
    }

    upsert (healthPassCheck) {
        return this.findOne({ where: { id: healthPassCheck.id } })
            .then((res) => {
                if (res) {
                    healthPassCheck.updatedAt = this.toolbox.datetime.now();
                    this.update(healthPassCheck);
                } else {
                    this.create(healthPassCheck);
                }
            });
    }
}

export = HealthPassCheckRepository;
