export = function (sequelize, DataTypes) {
    const HealthPassCheck = sequelize.define('HealthPassCheck', {
        id: {
            type: DataTypes.INTEGER(8).UNSIGNED,
            allowNull: false,
            primaryKey: true,
            field: 'idrj'
        },
        isValid: {
            type: DataTypes.TINYINT,
            allowNull: false,
            field: 'is_valid'
        },
        metadata: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'metadata'
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            onUpdate: DataTypes.NOW,
            allowNull: false,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        timestamps: false,
        tableName: 'health_pass_check'
    });

    HealthPassCheck.associate = function (models) {
        models.HealthPassCheck.belongsTo(models.MemberBooking, {
            foreignKey: 'id',
            targetKey: 'id',
            as: 'memberBooking'
        });
    };

    return HealthPassCheck;
};
