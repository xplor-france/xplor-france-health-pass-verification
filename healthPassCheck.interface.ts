export interface HealthPass {
    id: number,
    value: string,
    checkResponse?: HealthPassCheckResponse
}

interface HealthPassCheckResponse {
    liteValidityStatus: string,
    liteLastName: string,
    liteFirstName: string,
    liteDateOfBirth: string
}
